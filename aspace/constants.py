DEFAULT_API_HOST = 'http://localhost:8089'
DEFAULT_USERNAME = 'admin'
DEFAULT_PASSWORD = 'admin'

VALID_REPO_URI_REGEX = r'/?repositories/\d+'
VALID_USER_URI_REGEX = r'/?users/\d+'
VALID_ENUM_URI_REGEX = r'/?config/enumerations/\d+'
